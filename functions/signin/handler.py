# -*- coding: utf-8 -*-
from __future__ import print_function

import psycopg2

import json

import hashlib


def handler(event, context):
    Aut = event.get('initObj', None)
    Authen = Authenticate(Aut)
    if Authen == 1:
        have_user = event.get('userName', None)
        password = event.get('password', None)
        pass_encript = hashlib.new("sha1", password)
        have_pass = pass_encript.hexdigest()
        if have_user and\
           have_pass:
            return SignIn("{}".format(have_user),"{}".format(have_pass))
        else:
            errorMessage = {
                      "http_status": "BadRequest400",
                      "status_code": "1",
                      "error_message": "Parametros vacios o incorrectos"
            }
            raise Exception(json.dumps(errorMessage))
    else:
        errorMessage = {
                  "http_status": "BadRequest400",
                  "status_code": "1",
                  "error_message": "Tus credenciales no son correctas"
        }
        raise Exception(json.dumps(errorMessage))


def Authenticate(d):
    if d['SiteGuid'] == '687042' and\
        d['DomainID'] == 349938 and\
        d['ApiUser'] == 'tvpapi_167' and\
        d['ApiPass'] == '11111':
        return 1


def SignIn(userName,password):
    try:
        cadenaConexion="host='apisklic.cjepxpqdqhoc.us-west-2.rds.amazonaws.com' dbname='dbapisklic' user='apisklic' password='as#$kd#po29i'"
        obj = psycopg2.connect(cadenaConexion)
        objCursor = obj.cursor()
        objCursor.execute(
            """SELECT * FROM "user" WHERE "Email" = %s and "Password" = %s;""",(userName,password)
        )
        registros = objCursor.fetchone()
        objCursor.close()
        obj.close()
        u_Name = registros[1]
        u_LastName = registros[2]
        u_Birthday = registros[3]
        u_Email = registros[4]
        u_Password = registros[5]
        u_Nickname = registros[6]
        u_RegisterDate = registros[7]
        usuario = {
          "m_RespStatus": 21,
          "m_user": {
            "m_oBasicData": {
              "m_sUserName": "{}".format(u_Nickname),
              "m_sFirstName": "{}".format(u_Name),
              "m_sLastName": "{}".format(u_LastName),
              "m_sEmail": "{}".format(u_Email),
              "m_sAddress": "",
              "m_sCity": "",
              "m_State": {
                "m_nObjecrtID": 0,
                "m_sStateName": "",
                "m_sStateCode": "",
                "m_Country": {
                  "m_nObjecrtID": 0,
                  "m_sCountryName": "",
                  "m_sCountryCode": ""
                }
              },
              "m_Country": {
                "m_nObjecrtID": 0,
                "m_sCountryName": "",
                "m_sCountryCode": ""
              },
              "m_sZip": "",
              "m_sPhone": "",
              "m_sFacebookID": "",
              "m_sFacebookImage": "",
              "m_bIsFacebookImagePermitted": "false",
              "m_sAffiliateCode": "",
              "m_CoGuid": "",
              "m_ExternalToken": "",
              "m_sFacebookToken": "",
              "m_sTwitterToken": "",
              "m_sTwitterTokenSecret": "",
              "m_UserType": {
                "ID": "null",
                "Description": "",
                "IsDefault": "false"
              }
            },
            "m_oDynamicData": {
              "m_sUserData": [
                {
                  "m_sDataType": "Birthday",
                  "m_sValue": "{}".format(u_Birthday),
                },
                {
                  "m_sDataType": "IsOfflineMode",
                  "m_sValue": "true"
                }
              ]
            },
            "m_sSiteGUID": "5184996",
            "m_domianID": 0,
            "m_isDomainMaster": "false",
            "m_eUserState": 0,
            "m_nSSOOperatorID": 0,
            "m_eSuspendState": 0
          },
          "m_userInstanceID": "null"
        }
        return usuario
    except:
        errorMessage = {
                  "http_status": "BadRequest400",
                  "status_code": "1",
                  "error_message": "Unexpected Error Las credenciales proporcionadas no son correctas"
        }
        raise Exception(json.dumps(errorMessage))
