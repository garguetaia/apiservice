# -*- coding: utf-8 -*-
from __future__ import print_function

import psycopg2

import json

import hashlib


def handler(event, context):
    Aut = event.get('initObj', None)
    Authen = Authenticate(Aut)
    if Authen == 1:
        BasicData = event.get('userBasicData', None)
        have_m_sUserNameField = BasicData.get('m_sUserNameField', None)
        have_m_sFirstNameField = BasicData.get('m_sFirstNameField', None)
        have_m_sLastNameField = BasicData.get('m_sLastNameField', None)
        DynamicData = event.get('userDynamicData', None)
        UserData = DynamicData.get('m_sUserDataField', None)
        have_m_sValueField = Birthday(UserData)
        have_m_sEmailField = BasicData.get('m_sEmailField', None)
        have_sPassword = event.get('sPassword', None)
        if have_m_sUserNameField and\
           have_m_sFirstNameField and\
           have_m_sLastNameField and\
           have_m_sValueField and\
           have_m_sEmailField and\
           have_sPassword:
            return SignUp(
                    "{}".format(have_m_sUserNameField),
                    "{}".format(have_m_sFirstNameField),
                    "{}".format(have_m_sLastNameField),
                    "{}".format(have_m_sValueField),
                    "{}".format(have_m_sEmailField),
                    "{}".format(have_sPassword)
            )
        else:
            errorMessage = {
                      "http_status": "BadRequest400",
                      "status_code": "1",
                      "error_message": "Parametros vacios o incorrectos"
            }
            raise Exception(json.dumps(errorMessage))
    else:
        errorMessage = {
                  "http_status": "BadRequest400",
                  "status_code": "1",
                  "error_message": "Tus credenciales no son correctas"
        }
        raise Exception(json.dumps(errorMessage))


def Authenticate(d):
    if d['SiteGuid'] == '687042' and\
        d['DomainID'] == 349938 and\
        d['ApiUser'] == 'tvpapi_167' and\
        d['ApiPass'] == '11111':
        return 1


def Birthday(d):
    i = 0
    for n in d:
        i+=1
        for g in n.keys():
            if n[g] == 'Birthday':
                return n['m_sValueField']


def SignUp(
        m_sUserNameField,
        m_sFirstNameField,
        m_sLastNameField,
        m_sValueField,
        m_sEmailField,
        sPassword):
    cadenaConexion="host='apisklic.cjepxpqdqhoc.us-west-2.rds.amazonaws.com' dbname='dbapisklic' user='apisklic' password='as#$kd#po29i'"
    obj = psycopg2.connect(cadenaConexion)
    objCursor = obj.cursor()
    pass_encript = hashlib.new("sha1", sPassword)
    encript = pass_encript.hexdigest()
    objCursor.execute(
        """INSERT INTO "user" ("Name", "LastName", "Birthday", "Email", "Password", "Nickname", "RegisterDate")
            VALUES (%s, %s, %s, %s, %s, %s, now());""",
        (m_sFirstNameField, m_sLastNameField, m_sValueField, m_sEmailField, encript, m_sEmailField))
    obj.commit()
    objCursor.close()
    obj.close()
    registro = {
      "m_RespStatus": 21,
      "m_user": {
        "m_oBasicData": {
          "m_sUserName": "{}".format(m_sUserNameField),
          "m_sFirstName": "{}".format(m_sFirstNameField),
          "m_sLastName": "{}".format(m_sLastNameField),
          "m_sEmail": "{}".format(m_sEmailField),
          "m_sAddress": "",
          "m_sCity": "",
          "m_State": {
            "m_nObjecrtID": 0,
            "m_sStateName": "",
            "m_sStateCode": "",
            "m_Country": {
              "m_nObjecrtID": 0,
              "m_sCountryName": "",
              "m_sCountryCode": ""
            }
          },
          "m_Country": {
            "m_nObjecrtID": 0,
            "m_sCountryName": "",
            "m_sCountryCode": ""
          },
          "m_sZip": "",
          "m_sPhone": "",
          "m_sFacebookID": "",
          "m_sFacebookImage": "",
          "m_bIsFacebookImagePermitted": "false",
          "m_sAffiliateCode": "",
          "m_CoGuid": "",
          "m_ExternalToken": "",
          "m_sFacebookToken": "",
          "m_sTwitterToken": "",
          "m_sTwitterTokenSecret": "",
          "m_UserType": {
            "ID": "null",
            "Description": "",
            "IsDefault": "false"
          }
        },
        "m_oDynamicData": {
          "m_sUserData": [
            {
              "m_sDataType": "Birthday",
              "m_sValue": "{}".format(m_sValueField),
            },
            {
              "m_sDataType": "IsOfflineMode",
              "m_sValue": "true"
            }
          ]
        },
        "m_sSiteGUID": "5184996",
        "m_domianID": 0,
        "m_isDomainMaster": "false",
        "m_eUserState": 0,
        "m_nSSOOperatorID": 0,
        "m_eSuspendState": 0
      },
      "m_userInstanceID": "null"
    }
    return registro
